from controller import va, vb
import controller

app, db = controller.create_app(x={'name': __name__})

# Include Blueprints #
app.register_blueprint(va.va)
app.register_blueprint(vb.vb)


@app.errorhandler(404)
def page_not_found(e):
    return "I don't care what they say, tomato is not a fruit"


if __name__ == "__main__":
    app.run(port="5000")
