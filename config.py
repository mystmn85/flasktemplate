import os

basedir = os.path.abspath(os.path.dirname(__file__))
os.environ["DB_loc"] = "sqlite:///" + os.path.join(basedir, 'controller/test1.sqlite')


class Config:
    SecretKey = os.getenv('SecretKey', 'KIb2aHmlae8t37E6@5AbaSUSCr%pfHz%JjNZpe$k*AupozcY5jYimKOpmpTiCj5Whb*Badv!PcO')
    Bob = "hi!"


class DevelopmentConfig(Config):
    ENV = 'development'
    Debug = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, 'controller/main.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    Debug = True
    Testing = True
    ENV = "Testing"
    SQLALCHEMY_DATABASE_URI = os.getenv("TEST_DATABASE")
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    Debug = False
    ENV = 'production'


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)
