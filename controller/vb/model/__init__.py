from controller import create_app
import os

os.environ["DB_loc"] = "sqlite:///" + os.path.join(os.path.abspath(os.path.dirname(__file__)), '001_va.sqlite')

app, db = create_app(x={'name': __name__, 'model_path': 'yes'})


class Page(db.Model):
    __tablename__ = 'testing'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))

    def __init__(self, **kwargs):
        super(Page, self).__init__(**kwargs)

    def __repr__(self):
        return '<User {}>'.format(self.name)


def db_conn(x=None, data=None):
    if x is 'drop':
        db.drop_all()

    db.create_all()

    if x is 'insert':
        db.session.add(data)
        db.session.commit()

    db.session.close()
