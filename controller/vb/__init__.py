from flask import Blueprint, jsonify, render_template, url_for, redirect, Markup
import random, os
from controller.va import model
from controller.common import html

vb = Blueprint('vb', __name__, url_prefix='/vb', template_folder='templates', static_folder='static')
past = os.path.abspath(os.path.dirname(__file__))


def links():
    return "vb.results"


def loop_test():
    i = 1
    list_loop = []
    n, color = html.hex_colors()
    while i <= 5:
        i += 1
        list_loop.append(color[random.randrange(1, n)])
    return list_loop


def div_class():
    return Markup(
        f"<div class=\"row\">"
            f"<div class=\"col-sm-5\" style=\"background:{loop_test()[0]}\">Column</div>"
            f"<div class=\"col-sm-7\" style=\"background:{loop_test()[1]}\">Column</div>"
        f"</div>"
        f"<div class=\"row\">"
            f"<div class=\"col-sm-4\"style=\"background:{loop_test()[4]}\">Test, Test</div>"
            f"<div class=\"col-sm-4\"style=\"background:{loop_test()[2]}\">Column</div>"
            f"<div class=\"col-sm-4\"style=\"background:{loop_test()[3]}\">Column</div>"
        f"</div>"
    )


@vb.route("/homepage")
def logistics():
    return render_template('index.html')


@vb.route("/input")
def input():
    title = "Input page"
    return render_template('input.html', title=title, something=div_class())


@vb.route("/")
def index():
    mixed = random.randrange(100, 1000000)
    u = model.Page(name='john-{}'.format(mixed))
    model.db_conn('insert', u)
    return redirect(url_for('vb.results'))


@vb.route("/r")
def results():
    model.db_conn()
    pages = model.Page()
    posts = pages.query.order_by(pages.id).all()
    return render_template('results.html', users=posts)


@vb.route("/drop")
def drop_table():
    model.db_conn('drop')
    return redirect(url_for('vb.results'))


@vb.route("/d")
def env_page():
    return jsonify({'message': 'vblue for \'{}\''.format(past)})
