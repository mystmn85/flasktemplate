from flask import Blueprint, jsonify, render_template, url_for, redirect
import random, os
from controller.va import model

va = Blueprint('va', __name__, url_prefix='/va', template_folder='templates', static_folder='static')
past = os.path.abspath(os.path.dirname(__file__))


@va.route("/")
def index():
    mixed = random.randrange(100, 1000000)
    u = model.Page(name='john-{}'.format(mixed))
    model.db_conn('insert', u)
    return redirect(url_for('va.results'))


@va.route("/r")
def results():
    model.db_conn()
    pages = model.Page()
    posts = pages.query.order_by(pages.id).all()
    return render_template('results.html', users=posts)


@va.route("/drop")
def drop_table():
    model.db_conn('drop')
    return redirect(url_for('va.results'))


@va.route("/d")
def env_page():
    return jsonify({'message': 'value for \'{}\''.format(past)})
