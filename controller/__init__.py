from flask import Flask
import config
import os


def create_app(x=None, config=config.config_by_name['test']):
    from flask_sqlalchemy import SQLAlchemy

    if 'name' in x:
        app = Flask(x['name'])
    else:
        app = Flask(__name__)

    app.config.from_object(config)
    app.debug = True

    app.config.update(SQLALCHEMY_DATABASE_URI=os.getenv("DB_loc"))

    db = SQLAlchemy(app)

    with app.app_context():
        db.init_app(app)

    return app, db
